//
//  ViewController.m
//  TestExamenSRX
//
//  Created by Hiram Castro on 8/12/17.
//  Copyright © 2017 Hiram Castro. All rights reserved.
//

#import "ViewController.h"
#import "ViewCell.h"
#import "AFNetworking.h"
#import "ImageViewController.h"
#import "WebViewController.h"

@interface ViewController () <UICollectionViewDelegate, UICollectionViewDataSource> {

    IBOutlet UISegmentedControl *segmentedControl;
    IBOutlet UICollectionView *collectionView;
    
    NSArray *jsonArray;
    NSDictionary *jsonDictionary;
    NSMutableArray *dictionaryArray;
    NSString *cellIndentifier;
}

@end

@implementation ViewController

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self LoadBanner];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    cellIndentifier = @"testCell";//@"listCell";
    
    [self loadJSON];
}

#pragma mark - LoadBanner

-(void)LoadBanner
{
    [self.view addSubview:self.bannerView];
    [self.view bringSubviewToFront:self.bannerView];
    
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    //self.bannerView.adUnitID = @"ca-app-pub-1689924626678469/9510222154";
    self.bannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
    self.bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[ @"f378880396fc6b091f1622913c95d892" ];
    
    [self.bannerView loadRequest:[GADRequest request]];
}

#pragma mark - LoadJsonData

- (void)loadJSON
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:@"https://rallycoding.herokuapp.com/api/music_albums"
             parameters:nil
             progress:nil
             success:^(NSURLSessionTask *task, id responseObject)
             {
              NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseObject
                                                      options:NSJSONWritingPrettyPrinted
                                                      error:nil];
                 
              dictionaryArray = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                          options:NSJSONReadingMutableContainers
                                                                            error:nil];
              
              jsonDictionary = [NSJSONSerialization JSONObjectWithData:jsonData
                                                    options:NSJSONReadingMutableContainers
                                                    error:nil];
                 
              [collectionView reloadData];
                 
             }
             failure:^(NSURLSessionTask *operation, NSError *error)
             {
              NSLog(@"Error: %@", error);
             }];
}

#pragma mark - SegmentedControllAction

- (IBAction) segmentedControllPressedFilter:(id)sender
{
    UISegmentedControl *segmentedControll = (UISegmentedControl *)sender;
    
    if(segmentedControll.selectedSegmentIndex == 0)
    {
        cellIndentifier = @"testCell";
    }
    else
    {
        cellIndentifier = @"gridCell";
    }
    
    [collectionView reloadData];
}

#pragma mark - Open Link

-(IBAction)OpenLinkView:(UIButton *)UIButton
{
    ViewCell * cell = UIButton.superview.superview;
    
    NSIndexPath *indexPath = [collectionView indexPathForCell:cell];
    
    NSDictionary *dataCollection = [dictionaryArray objectAtIndex:indexPath.row];
    
    NSString *url = [dataCollection objectForKey:@"url"];
    
    [self performSegueWithIdentifier:@"toWebView" sender:url];
}

#pragma mark - CollectionViewDelegates

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return [jsonDictionary count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionViewJson
                            cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ViewCell *cell = [collectionViewJson dequeueReusableCellWithReuseIdentifier:cellIndentifier forIndexPath:indexPath];
    
    NSDictionary *dataCollection = [dictionaryArray objectAtIndex:indexPath.row];
    
    NSString *title = [dataCollection objectForKey:@"title"];
    NSString *artist = [dataCollection objectForKey:@"artist"];
    NSString *thumbnail_image = [dataCollection objectForKey:@"thumbnail_image"];
    
    cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnail_image]]];
    cell.headerLabel.text = artist;
    cell.contentLabel.text = title;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (segmentedControl.selectedSegmentIndex == 0) {
        return CGSizeMake(343, 100);
    }
    else {
        return CGSizeMake(150, 220);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dataCollection = [dictionaryArray objectAtIndex:indexPath.row];
    
    NSString *fullImageUrl = [dataCollection objectForKey:@"image"];
    
    [self performSegueWithIdentifier:@"fullImage" sender:fullImageUrl];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"fullImage"])
    {
        ImageViewController *fullImage = (ImageViewController*)segue.destinationViewController;
        fullImage.imgURL = sender;
    }
    
    if([segue.identifier isEqualToString:@"toWebView"])
    {
        WebViewController *toWebView = (WebViewController*)segue.destinationViewController;
        toWebView.webViewURL = sender;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
