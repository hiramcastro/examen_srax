//
//  ImageViewController.h
//  TestExamenSRX
//
//  Created by Hiram Castro on 8/13/17.
//  Copyright © 2017 Hiram Castro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (nonatomic, strong) NSString *imgURL;

@end
