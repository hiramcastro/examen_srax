//
//  ViewCell.h
//  TestExamenSRX
//
//  Created by Hiram Castro on 8/13/17.
//  Copyright © 2017 Hiram Castro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel *headerLabel;
@property (nonatomic, strong) IBOutlet UILabel *contentLabel;
@property (nonatomic, strong) IBOutlet UIButton *openLinkButton;

@end
