//
//  WebViewController.h
//  TestExamenSRX
//
//  Created by Hiram Castro on 8/17/17.
//  Copyright © 2017 Hiram Castro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *webViewURL;

@end
