//
//  ImageViewController.m
//  TestExamenSRX
//
//  Created by Hiram Castro on 8/13/17.
//  Copyright © 2017 Hiram Castro. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()
{
    IBOutlet UIImageView *imageView;
}

@end

@implementation ImageViewController

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self loadImage];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self AddClosingGesture];
    
    //[self addResizeGesture];
    
    //[self AddMovingGesture];
}

#pragma mark - Add Moving Gesture

-(void)AddMovingGesture
{
    UIPanGestureRecognizer *panRecognizer =
    [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [imageView addGestureRecognizer:panRecognizer];
}

#pragma mark - Add Close Gesture

-(void)AddClosingGesture
{
    UISwipeGestureRecognizer *swipeDown =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeDown];
}

#pragma mark - Add Resize Gesture

-(void)addResizeGesture
{
    UIPinchGestureRecognizer *pinchGesture =
    [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePiece:)];
    [imageView addGestureRecognizer:pinchGesture];
}

#pragma mark - Moving Method

-(void)move:(UIPanGestureRecognizer*)sender {
    [self.view bringSubviewToFront:sender.view];
    CGPoint translatedPoint = [sender translationInView:sender.view.superview];
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        CGFloat firstX = sender.view.center.x;
        CGFloat firstY = sender.view.center.y;
    }
    
    
    translatedPoint = CGPointMake(sender.view.center.x+translatedPoint.x, sender.view.center.y+translatedPoint.y);
    
    [sender.view setCenter:translatedPoint];
    [sender setTranslation:CGPointZero inView:sender.view];
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        CGFloat velocityX = (0.2*[sender velocityInView:self.view].x);
        CGFloat velocityY = (0.2*[sender velocityInView:self.view].y);
        
        CGFloat finalX = translatedPoint.x + velocityX;
        CGFloat finalY = translatedPoint.y + velocityY;// translatedPoint.y + (.35*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
        
        if (finalX < 0) {
            finalX = 0;
        } else if (finalX > self.view.frame.size.width) {
            finalX = self.view.frame.size.width;
        }
        
        if (finalY < 50) { // to avoid status bar
            finalY = 50;
        } else if (finalY > self.view.frame.size.height) {
            finalY = self.view.frame.size.height;
        }
        
        CGFloat animationDuration = (ABS(velocityX)*.0002)+.2;
        
        NSLog(@"the duration is: %f", animationDuration);
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidFinish)];
        [[sender view] setCenter:CGPointMake(finalX, finalY)];
        [UIView commitAnimations];
    }
}

#pragma mark - Close Method

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    [self closeImage];
}

#pragma mark - Resize Method

- (void)scalePiece:(UIPinchGestureRecognizer *)gestureRecognizer
{
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        CGFloat newScale = gestureRecognizer.scale;
        
        if(!(newScale < 1))
        {
         [gestureRecognizer view].transform =
         CGAffineTransformScale([[gestureRecognizer view] transform], [gestureRecognizer scale], [gestureRecognizer scale]);
        
         [gestureRecognizer setScale:1];
        }
    }
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        UIView *piece = gestureRecognizer.view;
        
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        CGPoint newPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        
        piece.layer.anchorPoint =
        CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        
        piece.center = locationInSuperview;
    }
}

#pragma mark - Close image Method

- (IBAction)closeImage {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Share Button Method

-(IBAction)ShareButton
{
    NSArray *activityItems = @[self.imgURL];
    
    UIActivityViewController *activityViewControntroller =
    [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    activityViewControntroller.excludedActivityTypes = @[];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewControntroller.popoverPresentationController.sourceView = self.view;
        activityViewControntroller.popoverPresentationController.sourceRect =
        CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    
    [self presentViewController:activityViewControntroller animated:true completion:nil];
}

#pragma mark - Load image Method

- (void)loadImage {
    
    imageView.image =
    [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imgURL]]];
}

@end
