//
//  WebViewController.m
//  TestExamenSRX
//
//  Created by Hiram Castro on 8/17/17.
//  Copyright © 2017 Hiram Castro. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()
{
    IBOutlet UIWebView *webView;
}
@end

@implementation WebViewController

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self loadWebView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Load WebView

-(void)loadWebView
{
    NSURL *url = [NSURL URLWithString:self.webViewURL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webView loadRequest:urlRequest];
}

#pragma mark - Close WebView

- (IBAction)closeWebView {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
