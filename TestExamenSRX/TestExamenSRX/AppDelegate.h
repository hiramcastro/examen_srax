//
//  AppDelegate.h
//  TestExamenSRX
//
//  Created by Hiram Castro on 8/12/17.
//  Copyright © 2017 Hiram Castro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

