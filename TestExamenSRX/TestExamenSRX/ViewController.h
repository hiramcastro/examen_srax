//
//  ViewController.h
//  TestExamenSRX
//
//  Created by Hiram Castro on 8/12/17.
//  Copyright © 2017 Hiram Castro. All rights reserved.
//

#import <UIKit/UIKit.h>

@import GoogleMobileAds;

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet GADBannerView *bannerView;

@end

